export { translateServiceSpy } from './translate-service';
export { platformSpy } from './platform';
export { settingsSpy } from './settings';
export { configSpy } from './config';
export { statusBarSpy } from './status-bar';
export { splashScreenSpy } from './splash-screen';
