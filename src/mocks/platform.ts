import { createSpyObj } from 'jest-createspyobj';
import { Platform } from 'ionic-angular';

export const platformSpy = createSpyObj(Platform, ['ready']);
platformSpy.ready.mockResolvedValue(null);
