import { createSpyObj } from 'jest-createspyobj';
import { TranslateService } from '@ngx-translate/core';

export const translateServiceSpy = createSpyObj(TranslateService, ['get', 'setDefaultLang', 'getBrowserLang', 'getBrowserCultureLang', 'use']);
translateServiceSpy.get.mockReturnValue({ subscribe: () => {} });
translateServiceSpy.getBrowserLang.mockReturnValue('en');
translateServiceSpy.getBrowserCultureLang.mockReturnValue('');
