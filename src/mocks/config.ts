import { createSpyObj } from 'jest-createspyobj';
import { Config } from 'ionic-angular';

export const configSpy = createSpyObj(Config);
