import { createSpyObj } from 'jest-createspyobj';
import { Settings } from '../providers';

export const settingsSpy = createSpyObj(Settings);
