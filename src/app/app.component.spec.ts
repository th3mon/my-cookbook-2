import {
  translateServiceSpy,
  platformSpy,
  settingsSpy,
  configSpy,
  statusBarSpy,
  splashScreenSpy
} from '../mocks';

import { MyApp } from './app.component';

describe('MyApp', () => {
  let app: MyApp;

  beforeEach(() => {
    app = new MyApp(
      translateServiceSpy,
      platformSpy,
      settingsSpy,
      configSpy,
      statusBarSpy,
      splashScreenSpy
    );
  });

  it('should create the root page', () => {
    expect(app).toBeTruthy();
  });

  describe('initTranslate()', () => {
    it('should use English language', () => {
      app.initTranslate();

      expect(translateServiceSpy.use).toHaveBeenCalledWith('en');
    });

    it('should use English language when getBrowserLang() returns null', () => {
      translateServiceSpy.getBrowserLang.mockReturnValue(null);

      app.initTranslate();

      expect(translateServiceSpy.use).toHaveBeenCalledWith('en');
    });

    it('should use Polish language', () => {
      translateServiceSpy.getBrowserLang.mockReturnValue('pl');
      app.initTranslate();

      expect(translateServiceSpy.use).toHaveBeenCalledWith('pl');
    });

    it('should use Mandarin Chinese (Simplified) language', () => {
      translateServiceSpy.getBrowserLang.mockReturnValue('zh');
      translateServiceSpy.getBrowserCultureLang.mockReturnValue('Hans');

      app.initTranslate();

      expect(translateServiceSpy.use).toHaveBeenCalledWith('zh-cmn-Hans');
    });

    it('should use Mandarin Chinese (Traditional) language', () => {
      translateServiceSpy.getBrowserLang.mockReturnValue('zh');
      translateServiceSpy.getBrowserCultureLang.mockReturnValue('Hant');

      app.initTranslate();

      expect(translateServiceSpy.use).toHaveBeenCalledWith('zh-cmn-Hant');
    });
  });
});
