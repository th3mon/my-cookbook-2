import { createSpyObj } from 'jest-createspyobj';
import { SplashScreen } from '@ionic-native/splash-screen';

export const splashScreenSpy = createSpyObj(SplashScreen);
