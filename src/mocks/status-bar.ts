import { createSpyObj } from 'jest-createspyobj';
import { StatusBar } from '@ionic-native/status-bar';

export const statusBarSpy = createSpyObj(StatusBar);
